package main

import (
	"fmt"

	"github.com/labstack/echo/v4"
)

func main() {
	app := echo.New()
	eventHandler := handler.eventHanddler{}
	app.Start(":3001")
	fmt.Println("working on port 3001")
}

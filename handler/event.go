package handler

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/zackb/UFCFightNightSchedule/view/event"
)

type eventHandler struct{}

func (h eventHandler) HandleEventShow(c echo.Context) error {
	return render(c, event.Show())
}
